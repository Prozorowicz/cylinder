public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(2.5);
        System.out.println("circle.radius= "+circle.getRadius());
        System.out.println("circle.area= "+circle.getArea());
        Circle badCircle = new Circle(-1);
        System.out.println("badCircle.radius= "+ badCircle.getRadius());
        Cylinder cylinder = new Cylinder(5,3.76);
        System.out.println("cylinder.radius= "+cylinder.getRadius());
        System.out.println("cylinder.height= "+cylinder.getHeight());
        System.out.println("cylinder.area= "+cylinder.getArea());
        System.out.println("cylinder.volume= "+cylinder.getVolume());
    }
}
